<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;

class Question extends Model
{
   protected $table = 'questions';


   /**
    * Get the answers with random order  that owned the question.
    */

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function results()
    {
        return $this->hasMany('App\Result');
    }



    public function answersFront()
    {
        return $this->hasMany('App\Answer')->inRandomOrder();
    }

   
}
