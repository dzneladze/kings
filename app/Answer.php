<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'answer', 'point', 'question_id'
    ];


    /**
     * Get the question that owns the answer.
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function results()
    {
        return $this->hasMany('App\Result');
    }
}
