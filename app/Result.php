<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{

	protected $table = 'results';

	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function answer()
    {
        return $this->belongsTo('App\Answer');
    }


    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
