<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test' , 'HomeController@index')->name('tests');
Route::post('/result', 'HomeController@result')->name('testResult');
Route::get('/download', 'HomeController@excel')->name('excel');

Route::get('/questions' , 'QuestionsController@index')->name('questions');
Route::get('/questions/form/{id}' , 'QuestionsController@form')->name('questionUpdForm');
Route::get('/questions/addNew' , 'QuestionsController@AddForm')->name('questionAddForm');
Route::post('/questions/insert' , 'QuestionsController@insert')->name('questionInsert');
Route::put('/questions/update/{id}' , 'QuestionsController@update')->name('questionUpdate');
Route::post('/questions/delete/{id}' , 'QuestionsController@delete')->name('questionDelete');

Route::auth();

