<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use Auth;
use App\Result;
use Carbon\Carbon;
use Excel;

class HomeController extends Controller
{

    /**
     * Show the tests dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$questions = Question::with('answers')->inRandomOrder()->get();

        return view('userside.test', compact('questions'));
    }

    public function result(Request $request)
    {

        $result = new Result;
        $user_id = Auth::user()->id;

        $data = [];


        foreach($request->request as $key => $value)
        {
            if ($key == '_token')
            {
              continue;
            }

            $singleAnswer = Answer::find($value);

            $answer_id = $singleAnswer->id;
            $question_id = $singleAnswer->question_id;
            $answer_point = $singleAnswer->point;

            $data[] = [
                 "question_id"=>$question_id,
                 "point" =>$answer_point, 
                 "user_id" =>$user_id , 
                 "answer_id"=>$value,
                 "created_at"=>Carbon::now(),
                 "updated_at"=>Carbon::now()
            ];
        }

        $result->insert($data);  // ბაზაში შენახვა შედეგების 



        $array = [];
        foreach($request->request as $key => $value)
        {
            $array[] = $value;
        }
        
        array_pop($array);
        
        $pointResult =  Answer::find($array)->sum('point');
        $questionCount = Question::count();


        return view('userside.result',compact('pointResult','questionCount'));
    
        
    }


    public function excel(){

    $result = Result::with('user','answer','question')->get();


    $data = array();

        foreach($result as $item){
            
            $item = [
                'userName'=>$item->user->name,
                'question'=>$item->question->question,
                'answer'=>$item->answer->answer,
                'answerPoint' => $item->answer->point
            ];
            $data[] = $item;           
        }

        $myFile = Excel::create('ExportedData', function($excel) use($data)
        {
            $excel->sheet('ExportFile', function($sheet) use($data)
            {
                $sheet->fromArray($data);
                $sheet->row(1, array('მომხმარებელი', 'კითხვა', 'პასუხი', 'ქულა'));
            });
        })->store('xls', 'excel');
        
        return redirect(asset('excel/ExportedData.xls'));
     
    }

}


