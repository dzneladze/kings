<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\Http\Requests;
use Carbon\Carbon;

class QuestionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$questions = Question::orderBy('id', 'DESC')->get();
        return view('backend.questions', compact('questions'));
    }

    public function form($id)
    {
        $question = Question::with('answers')->find($id);
    	return view('backend.questionsForm',compact('question'));
    }

    public function AddForm()
    {
    	return view('backend.questionsAddForm');
    }

    public function insert(Request $request)
    {

        $this->validate($request, [
            'question' => 'required|unique:questions',
            'correctAns' => 'required',
            'wrongAns1' => 'required',
            'wrongAns2' => 'required',
            'wrongAns3' => 'required',
        ]);

        $answer = new Answer;
        $question = new Question;
        
        $question -> question = $request->question;
        $question -> save();
       
        $data = [
            ["question_id"=>$question->id, 'point' => 1 , 'answer' => $request->correctAns, 'created_at' => Carbon::now() , 'updated_at' => Carbon::now()],
            ["question_id"=>$question->id, 'point' => 0 , 'answer' => $request->wrongAns1, 'created_at' => Carbon::now() , 'updated_at' => Carbon::now()],
            ["question_id"=>$question->id, 'point' => 0 , 'answer' => $request->wrongAns2, 'created_at' => Carbon::now() , 'updated_at' => Carbon::now()],
            ["question_id"=>$question->id, 'point' => 0 , 'answer' => $request->wrongAns3, 'created_at' => Carbon::now() , 'updated_at' => Carbon::now()],
        ];

        $answer -> insert($data);

        return redirect('questions');
    }


    public function update($id , Request $request)
    {

        $this->validate($request, [
            'question' => 'required',
            'correctAns' => 'required',
            'wrongAns1' => 'required',
            'wrongAns2' => 'required',
            'wrongAns3' => 'required',
        ]);

        $question = Question::with('answers')->find($id);
        $question->question = $request->question;
        $question->save();

        $answer = Answer::where('question_id' , $id)->get();

 
        $answer[0]->answer = $request->correctAns;
        $answer[1]->answer = $request->wrongAns1;
        $answer[2]->answer = $request->wrongAns2;
        $answer[3]->answer = $request->wrongAns3;


        foreach($answer as $answer)
        {
            $answer->save();
        }

        return redirect('questions');
    }

    public function delete($id)
    {
        Answer::where('question_id',$id)->delete();
        Question::where('id',$id)->delete();
        return "Deleted";
    }



}
