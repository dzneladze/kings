@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row">
        <table class="table table-bordered questionsSource">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">კითხვა</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($questions as $question)
              <tr>
                <th scope="row">{{ $question->id }}</th>
                <td class='questionTd'>{{ $question->question }}</td>
                <td class='answersTd'><a href="{{ url('/questions/form/'.$question->id) }}">რედაქტირება</a></td>
                <td class='optionTd{{ $question->id }}' onclick='deleteQuestion({{ $question->id }})'>წაშლა</td>
              </tr>
            @endforeach
            <tr>
              <td><a href="{{ Route('questionAddForm') }}">დამატება </a> </td>
            </tr>
          </tbody>
        </table>
    </div>
</div>


<script type='text/javascript'>
  function deleteQuestion(questionId)
  {
    $.ajax({
       type:'post',
       url:"{{ URL::to('/questions/delete') }}/"+questionId,
       data: {  "_token": "{{ csrf_token() }}"},
       success:function(data){
         $('.optionTd'+questionId).parent().hide();
       }
    });
  }
</script>
@endsection
