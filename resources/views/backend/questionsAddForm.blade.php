@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">ახალი კითხვის დამატება</div>
                <div class="panel-body">
                     @if ($errors->any())
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif
                    <form action = "{{ Route('questionInsert') }}" method ='post'>
                      <div class="form-group">
                        <label for="question">კითხვა</label>
                        <input type="text" name ='question' class="form-control" id="question" >
                      </div>
                      <div class="form-group">
                        <label for="corectAns">სწორი პასუხი</label>
                        <input type="text" name = 'correctAns' class="form-control" id="corectAns" >
                      </div>
                      <div class="form-group">
                        <label for="answer1">არასწორი პასუხი</label>
                        <input type="text" name = 'wrongAns1' class="form-control" id="answer1" >
                      </div>
                      <div class="form-group">
                        <label for="answer2">არასწორი პასუხი</label>
                        <input type="text" name ='wrongAns2' class="form-control" id="answer2" >
                      </div>
                      <div class="form-group">
                        <label for="answer3">არასწორი პასუხი</label>
                        <input type="text" name = 'wrongAns3' class="form-control" id="answer3" >
                      </div>
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-default">დამატება</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
