@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">შედეგები</div>
                <div class="panel-body">
                    <p>სულ მოცემული იყო {{ $questionCount }} კითხვა . თქვენ გაეცით სწორად პასუხი {{ $pointResult }} კითხვას ! </p> <br>
                    <a href="{{Route('excel')}}" class="btn btn-default">ტესტის ამობეჭდვა</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
