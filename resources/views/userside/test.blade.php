@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">გამოკითხვა</div>
                <div class="panel-body">
                    <form action="{{ Route('testResult') }}"  method='post' class='finalTest'>
                        @foreach($questions as $question)
                            <div class='singleQuestion'>
                              <div class="form-group">
                                <p class="help-block">
                                  {{ $question->question }}
                                </p>
                              </div>
                              <div class="checkbox">
                                    @foreach($question->answersFront as $answer)
                                         <label> <input type="radio" name="answer{{ $question->id }}" value='{{ $answer->id }}' > {{ $answer->answer }}  </label> 
                                    @endforeach
                              </div>
                            </div>
                            <hr>
                        @endforeach
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default">ტესტის დასრულება</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
