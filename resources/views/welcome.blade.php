@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">მოგესალმებით</div>
                <div class="panel-body">
                    ტესტირების დასაწყებათ გადადით შემდეგ მისამართზე  - >  <a href="{{ Route('tests') }}"> ტესტირება </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
